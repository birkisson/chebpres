f= chebfun(@(x) x.^4*(exp(-x))^2*(sin(x)).^6*(cos(x)).^5)
plot(f)
%%
f = chebfun(@(x) sin(x^2) + cos(x)^2,[0 10])
plot(f,'linewidth',2.5),set(gca,'fontsize',16), shg
%%
f =
   chebfun column (1 smooth piece)
       interval       length     endpoint values  
[       0,      10]      119         1      0.2 
%%
f([0.2 5 8.3])
ans =
    1.0005   -0.0519   -0.0371
%%
fp = diff(f)
plot(f,'linewidth',2.5),set(gca,'fontsize',16), shg
%%
fp =
   chebfun column (1 smooth piece)
       interval       length     endpoint values  
[       0,      10]      118  -3.1e-12       16 
vertical scale =  20 

%%
pc= plotcoeffs(f), set(gca,'fontsize',16)
set(pc,'markersize',11)
shg
%%
plot(cumsum(f),'linewidth',2.5),set(gca,'fontsize',16), shg
%%
f =
   chebfun column (1 smooth piece)
       interval       length     endpoint values  
[       0,      10]      119         1      0.2 

%%
f = chebfun(@(x) sin(x^2) + cos(x)^2,[0 10])
r = roots(f-0.5)
plot(f,'linewidth',2.5),set(gca,'fontsize',16),
hold on, plot(r, f(r),'.','markersize',16)

r =
    1.6188
    2.4817
    3.1541
    3.4888
    3.9593
    4.3886
    4.6341
    5.0529
    5.2990
    5.5962
    5.9105
    6.1003
    6.4296
    6.6006
    6.8783
    7.0914
    7.2930
    7.5479
    7.6939
    7.9588
    8.0941
    8.3312
    8.4918
    8.6808
    8.8752
    9.0183
    9.2361
    9.3513
    9.5711
    9.6850
    9.8842

%% von Mises
kappa = 2;
f = chebfun(@(x) exp(kappa*cos(x)),[-pi pi]);
density = f/sum(f);
clf
cdf = cumsum(density);
plot([density,cdf],'linewidth',2.5), axis([-pi pi 0 1])
title('von Mises distribution','fontsize',20)
legend('Density','Distribution','Location','northwest')
set(gca,'fontsize',16)
%%
cdfinv = inv(cdf);
plot(cdfinv,'linewidth',2.5)
title('Inverse of von Mises distribution','fontsize',20)
set(gca,'fontsize',16)

%%
u = rand(1e5,1);                           % uniform
x = cdfinv(u);                             % von Mises
[count,bin] = hist(x,40);
count = count/sum(count*(bin(2)-bin(1)));  % renormalize, total area = 1
cla, bar(bin,count), hold on
plot(density,'linewidth',2.5,'color', [0.8500    0.3250    0.0980]), axis tight
title('Sampled points and the original density','fontsize',20)
shg
set(gca,'fontsize',16)

%%
clf
N=chebop(@(theta) diff(theta,2)+sin(theta),[0 5])
N.lbc = 2;
N.rbc = 2;
u = N\0
plot(u,'linewidth',2)
shg

%%
chebgui
%%
%u'' - 0.1*u^3 = -(x>=0.9)*(x<=1.1)
clf
N=chebop(@(x,u) diff(u,2) - 0.1*u^3+(x>=.9)*(x<=1.1),[-5 5]);
N.lbc = @(u) diff(u);
N.rbc = 1;
%[u,info] = N\0
plot(u,'linewidth',2)
set(gca,'fontsize',12)
title('Solution of BVP','fontsize',18)
shg
%%
semilogy(info.normDelta,'-*','linewidth',2.5)
title('Convergence of Newton iteration','fontsize',18)
set(gca,'fontsize',12)

%%
for i=1:6
    subplot(2,3,i)
    plot(chebpoly(i),'linewidth',2)
    set(gca,'fontsize',12)
%%
clf
n = 1000;
tt= cos(pi*(0:n)/n)+1i*sin(pi*(0:n)/n);
hold on
plot(tt,'linewidth',2.5)

n = 16;
tt= cos(pi*(0:n)/n)+1i*sin(pi*(0:n)/n)
plot(tt,'.','markersize',18)
for i = 1:n+1
    plot([real(tt(i)) real(tt(i))],[0.02 max(0,imag(tt(i))-.02)],'k--','linewidth',1.5)
end
plot([-1 1],[0 0],'k','linewidth',2)
plot(cos(pi*(0:n)/n),0,'.','markersize',18,'color',[0.8500    0.3250    0.0980])

axis equal, axis off
%plot(tt,-cos(tt),'.')
shg
end